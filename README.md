# Tor theme for reveal.js

## About

This is a [reveal.js][] theme for [The Tor Project][].

[reveal.js]: https://revealjs.com

[The Tor Project]: https://torproject.org

## Requirements

To use this project, you'll need to have the following software:

* [Git](https://git-scm.com).
* [NPM](https://www.npmjs.com).
* [Gulp](https://gulpjs.com).
* Access to a terminal emulator in your system.

## Building

To use it, start by cloning this repository into a folder where you're going to
make your presentations. Example:

    git clone https://gitlab.torproject.org/juga/revealjs_tor_theme my_tor_presentations
    cd my_tor_presentations

Then install [reveal.js][] `reveal.js` by running the
following command in a terminal emulator:

    git submodule update --init --recursive

Copy `tor.scss` into `reveal.js/css/theme/source/`:

    cp tor.scss reveal.js/css/theme/source/

Go to the `reveal.js` folder, and build it:

    cd reveal.js
    npm install
    npm run build -- css-themes
    cd ..

Copy the `example.html`:

    cp example.html index.html

Edit `index.html` to suit your needs.

## Using

To try it, simply start a web server at the top level folder. Example:

    npx serve

You can also specify a custom host and port like this:

    npx serve --listen tcp://0.0.0.0:8000

## Pandoc support

This project also comes with a built-in [Pandoc][] support.

If [Pandoc][] is installed in your system, the [example.md](example.md) file can be
converted into an `index.html` using the following command:

    pandoc --template=templates/pandoc.template \
           -V theme=tor -s --mathml -i \
           -t revealjs -V revealjs-url=./reveal.js \
           -V base_path=. \
           --slide-level 2 example.md -o index.html

[Pandoc]: https://pandoc.org

## Customizing

* Copy the examples and change them to suit your needs.
* Check [this guide]() for customizing reveal.js.

[this guide]: https://github.com/hakimel/reveal.js/blob/master/css/theme/README.md
